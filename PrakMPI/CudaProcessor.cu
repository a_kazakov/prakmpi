#include "stdafx.h"

#include "CudaProcessor.h"

#include "CudaConsts.cuh"
#include "CudaFunctions.cuh"
#include "CudaTemplates.cuh"

#include <cuda.h>
#include <cuda_runtime_api.h>

#include <algorithm>
#include <stdexcept>


CudaProcessor::CudaProcessor(int height, int width, const Mesh &mesh, int device_number) : 
    mat_info(new CudaMatInfo()) 
{
    _setDeviceNumber(device_number);
    int full_height = height + 2;
    int full_width = width + 2;
    this->stride = full_width;
    this->height = height;
    this->width = width;
    mat_info->stride = stride;
    mat_info->height = height;
    mat_info->width = width;
    mat_info->full_height = full_height;
    mat_info->full_width = full_width;
    // Mesh
    mesh_y = _allocate_arr(full_height);
    mesh_x = _allocate_arr(full_width);
    CUDA_CHECK_ERROR(cudaMemcpy(mesh_y, mesh.raw_y(), full_height * sizeof(float), cudaMemcpyHostToDevice));
    CUDA_CHECK_ERROR(cudaMemcpy(mesh_x, mesh.raw_x(), full_width * sizeof(float), cudaMemcpyHostToDevice));
    // Buffers
    m_tmp_1 = _allocate_mat(full_height, full_width);
    m_tmp_2 = _allocate_mat(full_height, full_width);
    reduce_buffer_1 = _allocate_arr(REDUCER_BLOCKS);
    reduce_buffer_2 = _allocate_arr(1);
    buf_top = _allocate_arr(width);
    buf_bottom = _allocate_arr(width);
    buf_left = _allocate_arr(height);
    buf_right = _allocate_arr(height);
    // Actual matrixes
    m_right_part = _allocate_mat(full_height, full_width);
    m_solution = _allocate_mat(full_height, full_width);
    m_residual = _allocate_mat(full_height, full_width);
    m_basis = _allocate_mat(full_height, full_width);
}

CudaProcessor::~CudaProcessor() {
    CUDA_CHECK_ERROR(cudaFree(mesh_y));
    CUDA_CHECK_ERROR(cudaFree(mesh_x));
    CUDA_CHECK_ERROR(cudaFree(m_tmp_1));
    CUDA_CHECK_ERROR(cudaFree(m_tmp_2));
    CUDA_CHECK_ERROR(cudaFree(reduce_buffer_1));
    CUDA_CHECK_ERROR(cudaFree(reduce_buffer_2));
    CUDA_CHECK_ERROR(cudaFree(buf_top));
    CUDA_CHECK_ERROR(cudaFree(buf_bottom));
    CUDA_CHECK_ERROR(cudaFree(buf_left));
    CUDA_CHECK_ERROR(cudaFree(buf_right));
    CUDA_CHECK_ERROR(cudaFree(m_right_part));
    CUDA_CHECK_ERROR(cudaFree(m_solution));
    CUDA_CHECK_ERROR(cudaFree(m_residual));
    CUDA_CHECK_ERROR(cudaFree(m_basis));
    delete mat_info;
}

void CudaProcessor::importSolutionBounds(const float *top, const float *bottom, const float *left, const float *right) {
    return _importBounds(m_solution, top, bottom, left, right);
}

void CudaProcessor::importBasisBounds(const float *top, const float *bottom, const float *left, const float *right) {
    return _importBounds(m_basis, top, bottom, left, right);
}

void CudaProcessor::importResidualBounds(const float *top, const float *bottom, const float *left, const float *right) {
    return _importBounds(m_residual, top, bottom, left, right);
}

void CudaProcessor::exportSolutionBounds(float *top, float *bottom, float *left, float *right) {
    return _exportBounds(m_solution, top, bottom, left, right);
}

void CudaProcessor::exportBasisBounds(float *top, float *bottom, float *left, float *right) {
    return _exportBounds(m_basis, top, bottom, left, right);
}

void CudaProcessor::exportResidualBounds(float *top, float *bottom, float *left, float *right) {
    return _exportBounds(m_residual, top, bottom, left, right);
}

void CudaProcessor::computeRightPart() {
    NonOverlappedLaunchParams p(*mat_info);
    _dComputeRightPart<<<p.blocks, p.threads>>>(*mat_info, mat_info->pin(m_right_part), mesh_y + 1, mesh_x + 1);
}

void CudaProcessor::computeResidual() {
    OverlappedLaunchParams p(*mat_info);
    _dComputeResidual<<<p.blocks, p.threads>>>(*mat_info, m_residual, m_solution, m_right_part, mesh_y, mesh_x);
}

void CudaProcessor::findSpFromResidual(float &sp1, float &sp2) {
    OverlappedLaunchParams p(*mat_info);
    _dFindSpFromResidual<<<p.blocks, p.threads>>>(*mat_info, m_tmp_1, m_tmp_2, m_residual, mesh_y, mesh_x);
    sp1 = _reduceSum(m_tmp_1);
    sp2 = _reduceSum(m_tmp_2);
}

void CudaProcessor::findSpFromResidualAndBasis(float &sp1, float &sp2) {
    OverlappedLaunchParams p(*mat_info);
    _dFindSpFromResidualAndBasis<<<p.blocks, p.threads>>>(*mat_info, m_tmp_1, m_tmp_2, m_residual, m_basis, mesh_y, mesh_x);
    sp1 = _reduceSum(m_tmp_1);
    sp2 = _reduceSum(m_tmp_2);
}

void CudaProcessor::findAlpha(float &alpha) {
    OverlappedLaunchParams p(*mat_info);
    _dFindAlpha<<<p.blocks, p.threads>>>(*mat_info, m_tmp_1, m_residual, m_basis, mesh_y, mesh_x);
    alpha = _reduceSum(m_tmp_1);
}

void CudaProcessor::updateBasis(float alpha, float latest_sp) {
    NonOverlappedLaunchParams p(*mat_info);
    alpha /= latest_sp;
    _dUpdateBasis<<<p.blocks, p.threads>>>(*mat_info, mat_info->pin(m_basis), mat_info->pin(m_residual), alpha);
}

void CudaProcessor::updateSolutionFromResidual(float sp1, float sp2, float &err) {
    NonOverlappedLaunchParams p(*mat_info);
    const float tau = sp1 / sp2;
    _dUpdateSolution<<<p.blocks, p.threads>>>(
        *mat_info, 
        mat_info->pin(m_solution), 
        mat_info->pin(m_tmp_1), 
        mat_info->pin(m_residual), 
        tau
    );
    err = _reduceMax(m_tmp_1);
}

void CudaProcessor::updateSolutionFromBasis(float sp1, float sp2, float &err) {
    NonOverlappedLaunchParams p(*mat_info);
    const float tau = sp1 / sp2;
    _dUpdateSolution<<<p.blocks, p.threads>>>(
        *mat_info, 
        mat_info->pin(m_solution), 
        mat_info->pin(m_tmp_1), 
        mat_info->pin(m_basis), 
        tau
    );
    err = _reduceMax(m_tmp_1);    
}

void CudaProcessor::fillBasis() {
    CUDA_CHECK_ERROR(cudaMemcpy(m_basis, m_residual, (height + 2) + (width + 2), cudaMemcpyDeviceToDevice));
}

std::vector<float> CudaProcessor::exportSolutionValues() {
    std::vector<float> result, buffer;
    const int full_size = (2 + height) * (2 + width);
    result.reserve(width * height);
    buffer.reserve(full_size);
    CUDA_CHECK_ERROR(cudaMemcpy(&buffer.front(), m_solution, full_size * sizeof(float), cudaMemcpyDeviceToHost));
    for (int i = 1; i <= height; ++i) {
        for (int j = 1; j <= width; ++j) {
            result.push_back(buffer[i * stride + j]);
        }
    }
    return result;
}

void CudaProcessor::_setDeviceNumber(int num) {
    int dev_count;
    cudaGetDeviceCount(&dev_count);
    if (num >= dev_count) {
        FORCELOGALL("Requested more GPUs (" << (num + 1) << ") than available on host (" << dev_count << "). Performance may degrade.");
        num %= dev_count;
    }
    cudaSetDevice(num);
}

void CudaProcessor::_importBounds(float *dest, const float *top, const float *bottom, const float *left, const float *right) {
    const int n_blocks = (std::max(width, height) + BLOCK_SIZE_1D - 1) / BLOCK_SIZE_1D;
    CUDA_CHECK_ERROR(cudaMemcpy(buf_top, top, width * sizeof(float), cudaMemcpyHostToDevice));
    CUDA_CHECK_ERROR(cudaMemcpy(buf_bottom, bottom, width * sizeof(float), cudaMemcpyHostToDevice));
    CUDA_CHECK_ERROR(cudaMemcpy(buf_left, left, height * sizeof(float), cudaMemcpyHostToDevice));
    CUDA_CHECK_ERROR(cudaMemcpy(buf_right, right, height * sizeof(float), cudaMemcpyHostToDevice));
    _dImportBounds<<<n_blocks, BLOCK_SIZE_1D>>>(*mat_info, dest, buf_top, buf_bottom, buf_left, buf_right);
}

void CudaProcessor::_exportBounds(const float *src, float *top, float *bottom, float *left, float *right) {
    const int n_blocks = (std::max(width, height) + BLOCK_SIZE_1D - 1) / BLOCK_SIZE_1D;
    _dExportBounds<<<n_blocks, BLOCK_SIZE_1D>>>(*mat_info, src, buf_top, buf_bottom, buf_left, buf_right);
    CUDA_CHECK_ERROR(cudaMemcpy(top, buf_top, width * sizeof(float), cudaMemcpyDeviceToHost));
    CUDA_CHECK_ERROR(cudaMemcpy(bottom, buf_bottom, width * sizeof(float), cudaMemcpyDeviceToHost));
    CUDA_CHECK_ERROR(cudaMemcpy(left, buf_left, height * sizeof(float), cudaMemcpyDeviceToHost));
    CUDA_CHECK_ERROR(cudaMemcpy(right, buf_right, height * sizeof(float), cudaMemcpyDeviceToHost));
}

float CudaProcessor::_reduceSum(float *mat) {
    _dReduceSum<BLOCK_SIZE_1D><<<REDUCER_BLOCKS, BLOCK_SIZE_1D>>>(reduce_buffer_1, mat + stride + 1, stride, height * stride, width);
    _dReduceSum<REDUCER_BLOCKS><<<1, REDUCER_BLOCKS>>>(reduce_buffer_2, reduce_buffer_1, REDUCER_BLOCKS, REDUCER_BLOCKS, REDUCER_BLOCKS);
    float result = 0;
    CUDA_CHECK_ERROR(cudaMemcpy(&result, reduce_buffer_2, sizeof(float), cudaMemcpyDeviceToHost));
    return result;
}

float CudaProcessor::_reduceMax(float *mat) {
    _dReduceMax<BLOCK_SIZE_1D><<<REDUCER_BLOCKS, BLOCK_SIZE_1D>>>(reduce_buffer_1, mat + stride + 1, stride, height * stride, width);
    _dReduceMax<REDUCER_BLOCKS><<<1, REDUCER_BLOCKS>>>(reduce_buffer_2, reduce_buffer_1, REDUCER_BLOCKS, REDUCER_BLOCKS, REDUCER_BLOCKS);
    float result = 0;
    CUDA_CHECK_ERROR(cudaMemcpy(&result, reduce_buffer_2, sizeof(float), cudaMemcpyDeviceToHost));
    return result;
}

float *CudaProcessor::_allocate_mat(int full_height, int full_width) {
    float *result;
    CUDA_CHECK_ERROR(cudaMalloc(reinterpret_cast<void **>(&result), full_height * full_width * sizeof(float)));
    CUDA_CHECK_ERROR(cudaMemset(result, 0, full_height * full_width * sizeof(float)));
    return result;
}

float * CudaProcessor::_allocate_arr(int size) {
    float *result;
    CUDA_CHECK_ERROR(cudaMalloc(reinterpret_cast<void **>(&result), size * sizeof(float)));
    CUDA_CHECK_ERROR(cudaMemset(result, 0, size * sizeof(float)));
    return result;
}
