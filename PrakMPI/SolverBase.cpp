// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include "stdafx.h"
#include "SolverBase.h"

#include <algorithm>

SolverBase::SolverBase(const ChunkDescription& cd, Communicator& comm) :
    error(INFINITY),
    steps(0),
    _comm(comm),
    _cd(cd),
    _mesh(cd)
{
    _buffer_size = std::max(_cd.vnodes(), _cd.hnodes());
    for (int i = 0; i < 24; ++i) {
        _buffer.assign(24 * _buffer_size, 0.0f);
    }
    // Init boundary buffers
    if (!comm.hasTop()) {
        float *buf = getBuffer(NEXT_SOLUTION_TOP);
        for (int i = cd.left_node; i <= cd.right_node; ++i, ++buf) {
            *buf = get_border_value(_cd.g_top_value, _mesh.get_hpoint(i));
        }
    }
    if (!comm.hasBottom()) {
        float *buf = getBuffer(NEXT_SOLUTION_BOTTOM);
        for (int i = cd.left_node; i <= cd.right_node; ++i, ++buf) {
            *buf = get_border_value(_cd.g_bottom_value, _mesh.get_hpoint(i));
        }
    }
    if (!comm.hasLeft()) {
        float *buf = getBuffer(NEXT_SOLUTION_LEFT);
        for (int i = cd.top_node; i <= cd.bottom_node; ++i, ++buf) {
            *buf = get_border_value(_mesh.get_vpoint(i), _cd.g_left_value);
        }
    }
    if (!comm.hasRight()) {
        float *buf = getBuffer(NEXT_SOLUTION_RIGHT);
        for (int i = cd.top_node; i <= cd.bottom_node; ++i, ++buf) {
            *buf = get_border_value(_mesh.get_vpoint(i), _cd.g_right_value);
        }
    }
}

void SolverBase::_exchangeSolutionBounds() {
    _comm.sendToNeighbours(
        _cd.hnodes(),
        getBuffer(PREV_SOLUTION_TOP),
        _cd.hnodes(),
        getBuffer(PREV_SOLUTION_BOTTOM),
        _cd.vnodes(),
        getBuffer(PREV_SOLUTION_LEFT),
        _cd.vnodes(),
        getBuffer(PREV_SOLUTION_RIGHT)
    );
    _comm.recvFromNeighbours(
        _cd.hnodes(),
        getBuffer(NEXT_SOLUTION_TOP),
        _cd.hnodes(),
        getBuffer(NEXT_SOLUTION_BOTTOM),
        _cd.vnodes(),
        getBuffer(NEXT_SOLUTION_LEFT),
        _cd.vnodes(),
        getBuffer(NEXT_SOLUTION_RIGHT)
    );
}

void SolverBase::_exchangeResidualBounds() {
    _comm.sendToNeighbours(
        _cd.hnodes(),
        getBuffer(PREV_RESIDUAL_TOP),
        _cd.hnodes(),
        getBuffer(PREV_RESIDUAL_BOTTOM),
        _cd.vnodes(),
        getBuffer(PREV_RESIDUAL_LEFT),
        _cd.vnodes(),
        getBuffer(PREV_RESIDUAL_RIGHT)
    );
    _comm.recvFromNeighbours(
        _cd.hnodes(),
        getBuffer(NEXT_RESIDUAL_TOP),
        _cd.hnodes(),
        getBuffer(NEXT_RESIDUAL_BOTTOM),
        _cd.vnodes(),
        getBuffer(NEXT_RESIDUAL_LEFT),
        _cd.vnodes(),
        getBuffer(NEXT_RESIDUAL_RIGHT)
    );
}

void SolverBase::_exchangeBasisBounds() {
    _comm.sendToNeighbours(
        _cd.hnodes(),
        getBuffer(PREV_BASIS_TOP),
        _cd.hnodes(),
        getBuffer(PREV_BASIS_BOTTOM),
        _cd.vnodes(),
        getBuffer(PREV_BASIS_LEFT),
        _cd.vnodes(),
        getBuffer(PREV_BASIS_RIGHT)
    );
    _comm.recvFromNeighbours(
        _cd.hnodes(),
        getBuffer(NEXT_BASIS_TOP),
        _cd.hnodes(),
        getBuffer(NEXT_BASIS_BOTTOM),
        _cd.vnodes(),
        getBuffer(NEXT_BASIS_LEFT),
        _cd.vnodes(),
        getBuffer(NEXT_BASIS_RIGHT)
    );
}
