#include "cuda_taskdef.cuh"
#include "CudaFunctions.cuh"
#include "CudaMacro.cuh"
#include "CudaTemplates.cuh"

#include <cuda_runtime_api.h>
#include <cuda.h>

__device__ float _dCalcLeftPart(
    const float mat[BLOCK_SIZE_2D_Y][BLOCK_SIZE_2D_X], 
    int y,
    int x, 
    const CudaMeshPosition &mp
) {
    float cur = mat[y][x];
    return (
        ((cur - mat[y - 1][x]) / mp.phy - (mat[y + 1][x] - cur) / mp.hy) / mp.hhy +
        ((cur - mat[y][x - 1]) / mp.phx - (mat[y][x + 1] - cur) / mp.hx) / mp.hhx
    );
}

__device__ void _dLocateNonOverlappedPosition(
    const CudaMatInfo &mat_info,
    int &global_y, 
    int &global_x, 
    int &thread_y, 
    int &thread_x,
    bool &is_active
) {
    thread_y = threadIdx.y;
    thread_x = threadIdx.x;
    global_y = blockIdx.y * BLOCK_SIZE_2D_Y + thread_y;
    global_x = blockIdx.x * BLOCK_SIZE_2D_X + thread_x;
    is_active = global_y < mat_info.height && global_x < mat_info.width;
}

__device__ void _dLocateOverlappedPosition(
    const CudaMatInfo &mat_info,
    int &global_y, 
    int &global_x, 
    int &thread_y, 
    int &thread_x, 
    bool &is_active, 
    bool &is_border
) {
    thread_y = threadIdx.y;
    thread_x = threadIdx.x;
    int block_pin_y = blockIdx.y * (BLOCK_SIZE_2D_Y - 2);
    int block_pin_x = blockIdx.x * (BLOCK_SIZE_2D_X - 2);
    global_y = block_pin_y + thread_y;
    global_x = block_pin_x + thread_x;
    is_active = global_y < mat_info.full_height && global_x < mat_info.full_width;
    is_border = (
        thread_y == 0 || 
        thread_x == 0 || 
        thread_y == BLOCK_SIZE_2D_Y - 1 || 
        thread_x == BLOCK_SIZE_2D_X - 1 || 
        global_y == mat_info.full_height - 1 || 
        global_x == mat_info.full_width - 1
    );
}

__global__ void _dImportBounds(
    CudaMatInfo mat_info,
    float *mat, 
    const float *top, 
    const float *bottom, 
    const float *left, 
    const float *right
) {
    int idx = BLOCK_SIZE_1D * blockIdx.x + threadIdx.x;
    if (idx < mat_info.width) {
        mat_info.get(mat, 0, idx + 1) = top[idx];
        mat_info.get(mat, mat_info.height + 1, idx + 1) = bottom[idx];
    }
    if (idx < mat_info.height) {
        mat_info.get(mat, idx + 1, 0) = left[idx];
        mat_info.get(mat, idx + 1, mat_info.width + 1) = right[idx];
    }
}

__global__ void _dExportBounds(
    CudaMatInfo mat_info,
    const float *mat, 
    float *top, 
    float *bottom, 
    float *left, 
    float *right
) {
    int idx = BLOCK_SIZE_1D * blockIdx.x + threadIdx.x;
    if (idx < mat_info.width) {
        top[idx] = mat_info.get(mat, 1, idx + 1);
        bottom[idx] = mat_info.get(mat, mat_info.height, idx + 1);
    }
    if (idx < mat_info.height) {
        left[idx] = mat_info.get(mat, idx + 1, 1);
        right[idx] = mat_info.get(mat, idx + 1, mat_info.width);
    }
}

__global__ void _dComputeRightPart(
    CudaMatInfo mat_info,
    float *mat, 
    const float *mesh_y, 
    const float *mesh_x
) {
    NON_OVERLAPPED_GRID
    __shared__ float sh_mesh_y[BLOCK_SIZE_2D_Y];
    __shared__ float sh_mesh_x[BLOCK_SIZE_2D_X];
    // Init shared memory
    if (is_active) {
        if (thread_x == 0) { sh_mesh_y[thread_y] = mesh_y[global_y]; }
        if (thread_y == 0) { sh_mesh_x[thread_x] = mesh_x[global_x]; }
    }
    __syncthreads(); 
    // Do the job
    if (is_active) {
       mat_info.get(mat, global_y, global_x) = cuda_get_right_part(sh_mesh_y[thread_y], sh_mesh_x[thread_x]);
    }
}

__global__ void _dComputeResidual(
    CudaMatInfo mat_info,
    float *residual, 
    const float *solution, 
    const float *right_part, 
    float *mesh_y, 
    float *mesh_x
) {
    OVERLAPPED_GRID
    __shared__ float sh_solution[BLOCK_SIZE_2D_Y][BLOCK_SIZE_2D_X];
    __shared__ float sh_mesh_y[BLOCK_SIZE_2D_Y];
    __shared__ float sh_mesh_x[BLOCK_SIZE_2D_X];
    // Init shared memory
    if (is_active) {
        sh_solution[thread_y][thread_x] = mat_info.get(solution, global_y, global_x);
        if (thread_x == 0) { sh_mesh_y[thread_y] = mesh_y[global_y]; }
        if (thread_y == 0) { sh_mesh_x[thread_x] = mesh_x[global_x]; }
    }
    __syncthreads(); 
    // Do the job
    if (is_active && !is_border) {
        CudaMeshPosition mp;
        mp.set(sh_mesh_y + thread_y, sh_mesh_x + thread_x);
        mat_info.get(residual, global_y, global_x) = (
            _dCalcLeftPart(sh_solution, thread_y, thread_x, mp) - mat_info.get(right_part, global_y, global_x)
        );
    }
}

__global__ void _dFindSpFromResidual(
    CudaMatInfo mat_info,
    float *m_sp1, 
    float *m_sp2, 
    const float *residual, 
    const float *mesh_y, 
    const float *mesh_x
) {
    OVERLAPPED_GRID
    __shared__ float sh_residual[BLOCK_SIZE_2D_Y][BLOCK_SIZE_2D_X];
    __shared__ float sh_mesh_y[BLOCK_SIZE_2D_Y];
    __shared__ float sh_mesh_x[BLOCK_SIZE_2D_X];
    // Init shared memory
    float cur_residual;
    if (is_active) {
        cur_residual = mat_info.get(residual, global_y, global_x);
        sh_residual[thread_y][thread_x] = cur_residual;
        if (thread_x == 0) { sh_mesh_y[thread_y] = mesh_y[global_y]; }
        if (thread_y == 0) { sh_mesh_x[thread_x] = mesh_x[global_x]; }
    }
    __syncthreads();
    // Do the job
    if (is_active && !is_border) {
        CudaMeshPosition mp;
        mp.set(sh_mesh_y + thread_y, sh_mesh_x + thread_x);
        mat_info.get(m_sp1, global_y, global_x) = cur_residual * cur_residual * mp.s_ratio;
        mat_info.get(m_sp2, global_y, global_x) = _dCalcLeftPart(sh_residual, thread_y, thread_x, mp) * cur_residual * mp.s_ratio;
    }
}

__global__ void _dFindSpFromResidualAndBasis(
    CudaMatInfo mat_info,
    float *m_sp1, 
    float *m_sp2, 
    const float *residual,
    const float *basis,
    const float *mesh_y, 
    const float *mesh_x
) {
    OVERLAPPED_GRID
    __shared__ float sh_basis[BLOCK_SIZE_2D_Y][BLOCK_SIZE_2D_X];
    __shared__ float sh_mesh_y[BLOCK_SIZE_2D_Y];
    __shared__ float sh_mesh_x[BLOCK_SIZE_2D_X];
    // Load shared data
    float cur_basis;
    if (is_active) {
        cur_basis = mat_info.get(basis, global_y, global_x);
        sh_basis[thread_y][thread_x] = cur_basis;
        if (thread_x == 0) { sh_mesh_y[thread_y] = mesh_y[global_y]; }
        if (thread_y == 0) { sh_mesh_x[thread_x] = mesh_x[global_x]; }
    }
    __syncthreads();
    // Do the job
    if (is_active && !is_border) {
        CudaMeshPosition mp;
        mp.set(sh_mesh_y + thread_y, sh_mesh_x + thread_x);
        mat_info.get(m_sp1, global_y, global_x) = mat_info.get(residual, global_y, global_x) * cur_basis * mp.s_ratio;
        mat_info.get(m_sp2, global_y, global_x) = _dCalcLeftPart(sh_basis, thread_y, thread_x, mp) * cur_basis * mp.s_ratio;
    }
}

__global__ void _dFindAlpha(
    CudaMatInfo mat_info,
    float *m_alpha, 
    const float *residual, 
    const float *basis, 
    const float *mesh_y, 
    const float *mesh_x
) {
    OVERLAPPED_GRID
    __shared__ float sh_residual[BLOCK_SIZE_2D_Y][BLOCK_SIZE_2D_X];
    __shared__ float sh_mesh_y[BLOCK_SIZE_2D_Y];
    __shared__ float sh_mesh_x[BLOCK_SIZE_2D_X];
    // Load shared data
    if (is_active) {
        sh_residual[thread_y][thread_x] = mat_info.get(residual, global_y, global_x);
        if (thread_x == 0) { sh_mesh_y[thread_y] = mesh_y[global_y]; }
        if (thread_y == 0) { sh_mesh_x[thread_x] = mesh_x[global_x]; }
    }
    __syncthreads();
    // Do the job
    if (is_active && !is_border) {
        CudaMeshPosition mp;
        mp.set(sh_mesh_y + thread_y, sh_mesh_x + thread_x);
        mat_info.get(m_alpha, global_y, global_x) = (
            _dCalcLeftPart(sh_residual, thread_y, thread_x, mp) * 
            mat_info.get(basis, global_y, global_x) * 
            mp.s_ratio
        );
    }
}

__global__ void _dUpdateBasis(
    CudaMatInfo mat_info,
    float *basis,
    const float *residual, 
    float alpha
) {
    NON_OVERLAPPED_GRID
    if (is_active) {
        float &basis_ptr = mat_info.get(basis, global_y, global_x);
        basis_ptr = mat_info.get(residual, global_y, global_x) - alpha * basis_ptr;
    }
}

__global__ void _dUpdateSolution(
    CudaMatInfo mat_info,
    float *solution, 
    float *diff, 
    const float *residual_or_basis, 
    float tau
) {
    NON_OVERLAPPED_GRID
    if (is_active) {
        float &sol_ref = mat_info.get(solution, global_y, global_x);
        float next_val = sol_ref - tau * mat_info.get(residual_or_basis, global_y, global_x);
        mat_info.get(diff, global_y, global_x) = fabs(sol_ref  - next_val);
        sol_ref = next_val;
    }
}
