#pragma once

#include "taskdef.h"
#include "structs.h"
#include "Communicator.h"
#include "MatChunk.h"
#include "Mesh.h"

#include <vector>

class SolverBase {
public:
    float error;
    int steps;
    SolverBase(const ChunkDescription &cd, Communicator &comm);
    virtual ~SolverBase() {};
    virtual void solve(float eps, int max_sd_steps = 1, int max_gcm_steps = INT_MAX) = 0;
    virtual std::vector<float> exportResult() = 0;
protected:
    enum BufferType {
        PREV_SOLUTION_TOP,
        PREV_SOLUTION_BOTTOM,
        PREV_SOLUTION_LEFT,
        PREV_SOLUTION_RIGHT,
        PREV_RESIDUAL_TOP,
        PREV_RESIDUAL_BOTTOM,
        PREV_RESIDUAL_LEFT,
        PREV_RESIDUAL_RIGHT,
        PREV_BASIS_TOP,
        PREV_BASIS_BOTTOM,
        PREV_BASIS_LEFT,
        PREV_BASIS_RIGHT,
        NEXT_SOLUTION_TOP,
        NEXT_SOLUTION_BOTTOM,
        NEXT_SOLUTION_LEFT,
        NEXT_SOLUTION_RIGHT,
        NEXT_RESIDUAL_TOP,
        NEXT_RESIDUAL_BOTTOM,
        NEXT_RESIDUAL_LEFT,
        NEXT_RESIDUAL_RIGHT,
        NEXT_BASIS_TOP,
        NEXT_BASIS_BOTTOM,
        NEXT_BASIS_LEFT,
        NEXT_BASIS_RIGHT,
    };
    Communicator &_comm;
    const ChunkDescription _cd;
    Mesh _mesh;
    std::vector<float> _buffer;
    int _buffer_size;

    inline float *getBuffer(BufferType type) {
        return &(_buffer[_buffer_size * static_cast<int>(type)]);
    }

    void _exchangeSolutionBounds();
    void _exchangeResidualBounds();
    void _exchangeBasisBounds();
};

