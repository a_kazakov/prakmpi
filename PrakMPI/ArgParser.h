#pragma once

#include <map>
#include <set>
#include <vector>

class ArgParser {
public:
    ArgParser(int argc, char *argv[]);
    int getMeshHeight();
    int getMeshWidth();
    int getMaxSDSteps();
    int getMaxGCMSteps();
    int getOmpThreads();
    std::string getOutputFileName();
    bool useSingleProcessor();
    bool useCuda();
protected:
    std::map<std::string, std::string> _kwargs;
    std::set<std::string> _flags;
    std::vector<std::string> _args;
private:
    int strToInt(const std::string &s);
};
