// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include "stdafx.h"
#include "MatChunk.h"

MatChunk::MatChunk(const ChunkDescription &cd) :
    _cd(cd),
    _mat((2 + cd.hnodes()) * (2 + cd.vnodes())),
    _stride(2 + cd.hnodes()),
    _yoffset(cd.top_node - 1),
    _xoffset(cd.left_node - 1) 
{}

void MatChunk::importBounds(const float* top, const float *bottom, const float *left, const float *right) {
    std::stringstream ss;
    for (int i = _cd.left_node; i <= _cd.right_node; ++i, ++top) {
        (*this)(_cd.top_node - 1, i) = *top;
    }
    for (int i = _cd.left_node; i <= _cd.right_node; ++i, ++bottom) {
        (*this)(_cd.bottom_node + 1, i) = *bottom;
    }
    for (int i = _cd.top_node; i <= _cd.bottom_node; ++i, ++left) {
        (*this)(i, _cd.left_node - 1) = *left;
    }
    for (int i = _cd.top_node; i <= _cd.bottom_node; ++i, ++right) {
        (*this)(i, _cd.right_node + 1) = *right;
    }
}

void MatChunk::exportBounds(float* top, float *bottom, float *left, float *right) const {
    for (int i = _cd.left_node; i <= _cd.right_node; ++i, ++top) {
        *top = (*this)(_cd.top_node, i);
    }
    for (int i = _cd.left_node; i <= _cd.right_node; ++i, ++bottom) {
        *bottom = (*this)(_cd.bottom_node, i);
    }
    for (int i = _cd.top_node; i <= _cd.bottom_node; ++i, ++left) {
        *left = (*this)(i, _cd.left_node);
    }
    for (int i = _cd.top_node; i <= _cd.bottom_node; ++i, ++right) {
        *right = (*this)(i, _cd.right_node);
    }
}

void MatChunk::assign(MatChunk &mc) {
    _mat.assign(mc._mat.begin(), mc._mat.end());
}
