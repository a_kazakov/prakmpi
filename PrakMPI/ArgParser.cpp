// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include "stdafx.h"
#include "ArgParser.h"

#include <stdexcept>

ArgParser::ArgParser(int argc, char *argv[]) {
    if (argc < 2) {
        throw std::runtime_error(
            "Invalid number of arguments.\n\n"
            "Usage: ./PrakMPI <v_nodes> <h_nodes>\n"
            "[--sd-steps <sd_max_steps>] [--gcm-steps <gcm_max_steps>]\n"
            "[--omp-threads <omp_threads>] [--single-proc | -s] [--out <output_file>]\n");
    }
    for (int i = 1; i < argc; ++i) {
        if (argv[i][0] == '-') { // flags
            if (argv[i][1] == '-') { // one flag
                if (argc > i + 1 && argv[i + 1][0] != '-') {
                    _kwargs[std::string(argv[i] + 2)] = argv[i + 1];
                    ++i;
                } else {
                    _flags.insert(std::string(argv[i] + 2));
                }
            } else {
                for (char *ptr = argv[i] + 1; *ptr; ++ptr) {
                    _flags.insert(std::string(size_t(1), *ptr));
                }
            }
        } else {
            std::string arg(argv[i]);
            _args.push_back(arg);
        }
    }
}

int ArgParser::getMeshHeight() {
    return strToInt(_args[1]);
}

int ArgParser::getMeshWidth() {
    return strToInt(_args[0]);
}

int ArgParser::getMaxSDSteps() {
    return _kwargs.count("sd-steps") > 0 ? strToInt(_kwargs["sd-steps"]) : 1;
}

int ArgParser::getMaxGCMSteps() {
    return _kwargs.count("gcm-steps") > 0 ? strToInt(_kwargs["gcm-steps"]) : INT_MAX;
}

int ArgParser::getOmpThreads() {
    return _kwargs.count("omp-threads") > 0 ? strToInt(_kwargs["omp-threads"]) : 1;
}

std::string ArgParser::getOutputFileName() {
    return _kwargs.count("out") > 0 ? _kwargs["out"] : "";
}

bool ArgParser::useSingleProcessor() {
    return _flags.count("single-proc") > 0 || _flags.count("s") > 0;
}

bool ArgParser::useCuda() {
    return _kwargs.count("device") > 0 ? _kwargs["device"] == "cuda" : false;
}

int ArgParser::strToInt(const std::string &s) {
    std::stringstream ss(s);
    int res;
    ss >> res;
    return res;
}
