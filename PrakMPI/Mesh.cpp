// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include "stdafx.h"
#include "Mesh.h"

#include <cmath>

Mesh::Mesh(const ChunkDescription &cd) :
    voffset(cd.top_node - 1),
    hoffset(cd.left_node - 1),
    vpoints(cd.bottom_node - cd.top_node + 3),
    hpoints(cd.right_node - cd.left_node + 3) 
{
    for (int i = cd.top_node - 1; i <= cd.bottom_node + 1; ++i) {
        vpoints[i - voffset] = (
            cd.g_bottom_value * f(float(i) / cd.g_bottom_node) + 
            cd.g_top_value * (1 - f(float(i) / cd.g_bottom_node))
        );
    }
    for (int i = cd.left_node - 1; i <= cd.right_node + 1; ++i) {
        hpoints[i - hoffset] = (
            cd.g_right_value * f(float(i) / cd.g_right_node) +
            cd.g_left_value * (1 - f(float(i) / cd.g_right_node))
        );
    }
}

inline float Mesh::f(float t) {
    static const float q = 2.0f / 3.0f;
    return (std::pow(1.0f + t, q) - 1) / (std::pow(2.0f, q) - 1);
}
