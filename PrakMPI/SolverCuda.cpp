// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include "stdafx.h"
#include "SolverCuda.h"

SolverCuda::SolverCuda(const ChunkDescription &cd, Communicator &comm) : 
    SolverBase(cd, comm),
    _cp(cd.vnodes(), cd.hnodes(), _mesh, _comm.getHostRank()),
    _latest_sp(0)
{
    _cp.computeRightPart();
}

void SolverCuda::solve(float eps, int max_sd_steps, int max_gcm_steps) {
    error = INFINITY;
    steps = 0;
    for (int i = 0; i < max_sd_steps && error > eps; ++i) {
        ++steps;
        steepDescentStep(error);
        LOG(_comm, "Performed step " << steps << " (algo=SD, error=" << error << ")");
    }
    _cp.fillBasis();
    for (int i = 0; i < max_gcm_steps && error > eps; ++i) {
        ++steps;
        gcmStep(error);
        LOG(_comm, "Performed step " << steps << " (algo=GCM, error=" << error << ")");
    }
}

std::vector<float> SolverCuda::exportResult() {
    std::vector<float> result;
    result.reserve(3 * _cd.hnodes() * _cd.vnodes());
    std::vector<float> values = _cp.exportSolutionValues();
    std::vector<float>::iterator val_iter = values.begin();
    for (int i = _cd.top_node; i <= _cd.bottom_node; ++i) {
        for (int j = _cd.left_node; j <= _cd.right_node; ++j) {
            result.push_back(_mesh.get_vpoint(i));
            result.push_back(_mesh.get_hpoint(j));
            result.push_back(*(val_iter++));
        }
    }    
    return result;
}

void SolverCuda::updateSolutionBounds() {
    _cp.exportSolutionBounds(
        getBuffer(PREV_SOLUTION_TOP),
        getBuffer(PREV_SOLUTION_BOTTOM),
        getBuffer(PREV_SOLUTION_LEFT),
        getBuffer(PREV_SOLUTION_RIGHT)
    );
    _exchangeSolutionBounds();
    _cp.importSolutionBounds(
        getBuffer(NEXT_SOLUTION_TOP),
        getBuffer(NEXT_SOLUTION_BOTTOM),
        getBuffer(NEXT_SOLUTION_LEFT),
        getBuffer(NEXT_SOLUTION_RIGHT)
    );
}

void SolverCuda::updateResidualBounds() {
    _cp.exportResidualBounds(
        getBuffer(PREV_RESIDUAL_TOP),
        getBuffer(PREV_RESIDUAL_BOTTOM),
        getBuffer(PREV_RESIDUAL_LEFT),
        getBuffer(PREV_RESIDUAL_RIGHT)
    );
    _exchangeResidualBounds();
    _cp.importResidualBounds(
        getBuffer(NEXT_RESIDUAL_TOP),
        getBuffer(NEXT_RESIDUAL_BOTTOM),
        getBuffer(NEXT_RESIDUAL_LEFT),
        getBuffer(NEXT_RESIDUAL_RIGHT)
    );
}

void SolverCuda::updateBasisBounds() {
    _cp.exportBasisBounds(
        getBuffer(PREV_BASIS_TOP),
        getBuffer(PREV_BASIS_BOTTOM),
        getBuffer(PREV_BASIS_LEFT),
        getBuffer(PREV_BASIS_RIGHT)
    );
    _exchangeBasisBounds();
    _cp.importBasisBounds(
        getBuffer(NEXT_BASIS_TOP),
        getBuffer(NEXT_BASIS_BOTTOM),
        getBuffer(NEXT_BASIS_LEFT),
        getBuffer(NEXT_BASIS_RIGHT)
    );
}

void SolverCuda::steepDescentStep(float &current_error) {
    updateSolutionBounds();
    _cp.computeResidual();
    updateResidualBounds();
    float sp1, sp2;
    _cp.findSpFromResidual(sp1, sp2);
    sp1 = _comm.reduceSum(sp1);
    sp2 = _comm.reduceSum(sp2);
    _latest_sp = sp2;
    _cp.updateSolutionFromResidual(sp1, sp2, current_error);
    current_error = _comm.reduceMaximum(current_error);
}

void SolverCuda::gcmStep(float &current_error) {
    updateSolutionBounds();
    _cp.computeResidual();
    updateResidualBounds();
    float alpha;
    _cp.findAlpha(alpha);
    alpha = _comm.reduceSum(alpha);
    _cp.updateBasis(alpha, _latest_sp);
    updateBasisBounds();
    float sp1, sp2;
    _cp.findSpFromResidualAndBasis(sp1, sp2);
    sp1 = _comm.reduceSum(sp1);
    sp2 = _comm.reduceSum(sp2);
    _latest_sp = sp2;
    _cp.updateSolutionFromBasis(sp1, sp2, current_error);
    current_error = _comm.reduceMaximum(current_error);
}
