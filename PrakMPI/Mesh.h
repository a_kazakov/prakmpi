#pragma once

#include "structs.h"

#include <vector>

class Mesh {
public:
    Mesh(const ChunkDescription &cd);
    inline int width() {
        return int(hpoints.size()) - 1;
    }
    inline int height() {
        return int(vpoints.size()) - 1;
    }
    inline float get_hpoint(int idx) {
        return hpoints[idx - hoffset];
    }
    inline float get_vpoint(int idx) {
        return vpoints[idx - voffset];
    }
    inline float hx(int i) {
        return get_hpoint(i + 1) - get_hpoint(i);
    }
    inline float hy(int i) {
        return get_vpoint(i + 1) - get_vpoint(i);
    }
    inline float hhx(int i) {
        return (hx(i) + hx(i - 1)) * 0.5f;
    }
    inline float hhy(int i) {
        return (hy(i) + hy(i - 1)) * 0.5f;
    }
    inline float s_ratio(int y, int x) {
        return hhy(y) * hhx(x);
    }
    inline const float *raw_y() const {
        return &vpoints.front();
    }
    inline const float *raw_x() const{
        return &hpoints.front();
    }
private:
    int voffset, hoffset;
    std::vector<float> vpoints, hpoints;
    inline float f(float t);
};
