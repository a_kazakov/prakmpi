#pragma once

#include <vector>
#include <mpi.h>

class Communicator {
public:
    MPI_Comm comm;
    int nprocs;
    int y, x;
    Communicator(int &argc, char **&argv);
    void init(int height, int width, bool single_processor);
    void finalize();
    int getHostRank();
    inline bool isPrimary() { return _rank == 0; }
    inline bool isActive() { return _rank >= 0; }
    inline bool hasTop() { return y > 0; }
    inline bool hasBottom() { return y < _height - 1; }
    inline bool hasLeft() { return x > 0; }
    inline bool hasRight() { return x < _width - 1; }
    void sendToNeighbours(
        int top_length,
        float *top_values,
        int bottom_length,
        float *bottom_values,
        int left_length,
        float *left_values,
        int right_length,
        float *right_values
    );
    void recvFromNeighbours(
        int top_length,
        float *top_values,
        int bottom_length,
        float *bottom_values,
        int left_length,
        float *left_values,
        int right_length,
        float *right_values
    );
    float reduceMaximum(float value);
    float reduceSum(float value);
    void barrier();
    void sendResult(std::vector<float> &data);
    std::vector<float> fetchResult();
private:
    enum Tag {
        TOP = 1,
        BOTTOM = 2,
        LEFT = 3,
        RIGHT = 4,
        GATHER_SIZE = 5,
        GATHER_DATA = 6,
    };
    int _height, _width;
    int _left, _right, _top, _bottom;
    int _wrank, _rank;
    bool _single_processor;
    void sendIfHasNeighbour(int neighbour_rank, Tag tag, int length, float *values);
    bool recvIfHasNeighbour(int neighbout_rank, Tag tag, int length, float *values, MPI_Request &req);
};
