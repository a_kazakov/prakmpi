#pragma once

#include "CudaConsts.cuh"

#include <cuda_runtime_api.h>
#include <cuda.h>

template<int block_size>
__global__ void _dReduceSum(
    float *dest, 
    const float *mat, 
    int stride, 
    int size, 
    int row_limit
) {
    __shared__ float sdata[BLOCK_SIZE_1D];
    int tid = threadIdx.x;
    int i = blockIdx.x * block_size + tid;
    int grid_size = block_size * gridDim.x;
    sdata[tid] = 0.0f;
    while (i < size) {
        if (i % stride < row_limit) {
            sdata[tid] += mat[i];
        }
        i += grid_size;
    }
    __syncthreads();
#define STEP(x) if (block_size >= x) { if (tid < x / 2) { sdata[tid] += sdata[tid + x / 2]; } __syncthreads(); }
    STEP(512);
    STEP(256);
    STEP(128);
#undef STEP
#define STEP(x) if (block_size >= x) { sdata[tid] += sdata[tid + x / 2]; __syncthreads(); }
    if (tid < 32) {
        STEP(64);
        STEP(32);
        STEP(16);
        STEP(8);
        STEP(4);
        STEP(2);
    }
#undef STEP
    if (tid == 0) {
        dest[blockIdx.x] = sdata[0];
    }
}

template<int block_size>
__global__ void _dReduceMax(
    float *dest, 
    float *mat, 
    int stride, 
    int size, 
    int row_limit
) {
    __shared__ float sdata[BLOCK_SIZE_1D];
    int tid = threadIdx.x;
    int i = blockIdx.x * (2 * block_size) + tid;
    int grid_size = 2 * block_size * gridDim.x;
    sdata[tid] = 0.0f;
    while (i < size) {
        if (i % stride < row_limit) {
            sdata[tid] = max(sdata[tid], mat[i]);
        }
        i += grid_size;
    }
    __syncthreads();
#define STEP(x) if (block_size >= x) { if (tid < x / 2) { sdata[tid] = max(sdata[tid], sdata[tid + x / 2]); } __syncthreads(); }
    STEP(512);
    STEP(256);
    STEP(128);
#undef STEP
#define STEP(x) if (block_size >= x) { sdata[tid] = max(sdata[tid], sdata[tid + x / 2]);  __syncthreads(); }
    if (tid < 32) {
        STEP(64);
        STEP(32);
        STEP(16);
        STEP(8);
        STEP(4);
        STEP(2);
    }
#undef STEP
    if (tid == 0) {
        dest[blockIdx.x] = sdata[0];
    }
}
