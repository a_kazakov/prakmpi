#pragma once

#include "structs.h"

#include <vector>

class MatChunk {
public:
    MatChunk(const ChunkDescription &cd);
    inline float &operator()(int y, int x) {
        return _mat[(y - _yoffset) * _stride + (x - _xoffset)];
    }
    inline float operator()(int y, int x) const {
        return _mat[(y - _yoffset) * _stride + (x - _xoffset)];
    }
    void importBounds(const float *top, const float *bottom, const float *left, const float *right);
    void exportBounds(float* top, float *bottom, float *left, float *right) const;
    void assign(MatChunk &mc);
//private:
    ChunkDescription _cd;
    std::vector<float> _mat;
    int _stride, _yoffset, _xoffset;
};
