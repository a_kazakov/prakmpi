#pragma once

#include "cuda_taskdef.cuh"

#include <cuda_runtime_api.h>
#include <cuda.h>

#include "CudaConsts.cuh"
#include "CudaStructs.cuh"

__device__ float _dCalcLeftPart(
    const float mat[BLOCK_SIZE_2D_Y][BLOCK_SIZE_2D_X], 
    int y, 
    int x, 
    const CudaMeshPosition &mp
);

__device__ void _dLocateNonOverlappedPosition(
    const CudaMatInfo &mat_info,
    int &global_y, 
    int &global_x, 
    int &thread_y, 
    int &thread_x,
    bool &is_active
);

__device__ void _dLocateOverlappedPosition(    
    const CudaMatInfo &mat_info,
    int &global_y, 
    int &global_x, 
    int &thread_y, 
    int &thread_x, 
    bool &is_active, 
    bool &is_border
);

__global__ void _dImportBounds(
    CudaMatInfo mat_info,
    float *mat, 
    const float *top, 
    const float *bottom, 
    const float *left, 
    const float *right
);

__global__ void _dExportBounds(
    CudaMatInfo mat_info,
    const float *mat, 
    float *top, 
    float *bottom, 
    float *left, 
    float *right
);

__global__ void _dComputeRightPart(
    CudaMatInfo mat_info,
    float *mat, 
    const float *mesh_y, 
    const float *mesh_x
);

__global__ void _dComputeResidual(
    CudaMatInfo mat_info,
    float *residual, 
    const float *solution, 
    const float *right_part, 
    float *mesh_y, 
    float *mesh_x
);

__global__ void _dFindSpFromResidual(
    CudaMatInfo mat_info,
    float *m_sp1, 
    float *m_sp2, 
    const float *residual, 
    const float *mesh_y, 
    const float *mesh_x
);

__global__ void _dFindSpFromResidualAndBasis(
    CudaMatInfo mat_info,
    float *m_sp1,
    float *m_sp2,
    const float *residual,
    const float *basis,
    const float *mesh_y,
    const float *mesh_x
);

__global__ void _dFindAlpha(
    CudaMatInfo mat_info,
    float *m_alpha, 
    const float *residual,
    const float *basis,
    const float *mesh_y,
    const float *mesh_x
);

__global__ void _dUpdateBasis(
    CudaMatInfo mat_info,
    float *basis,
    const float *residual, 
    float alpha
);

__global__ void _dUpdateSolution(
    CudaMatInfo mat_info,
    float *solution, 
    float *diff, 
    const float *residual_or_basis, 
    float tau
);
