#pragma once

#include "Mesh.h"

struct CudaMatInfo;

class CudaProcessor {
public:
    CudaProcessor(int height, int width, const Mesh &mesh, int device_number);
    ~CudaProcessor();
    void importSolutionBounds(const float *top, const float *bottom, const float *left, const float *right);
    void importBasisBounds(const float *top, const float *bottom, const float *left, const float *right);
    void importResidualBounds(const float *top, const float *bottom, const float *left, const float *right);
    void exportSolutionBounds(float *top, float *bottom, float *left, float *right);
    void exportBasisBounds(float *top, float *bottom, float *left, float *right);
    void exportResidualBounds(float *top, float *bottom, float *left, float *right);
    void computeRightPart();
    void computeResidual();
    void findSpFromResidual(float &sp1, float &sp2);
    void findSpFromResidualAndBasis(float &sp1, float &sp2);
    void findAlpha(float &alpha);
    void updateBasis(float alpha, float latest_sp);
    void updateSolutionFromResidual(float sp1, float sp2, float &err);
    void updateSolutionFromBasis(float sp1, float sp2, float &err);
    void fillBasis();
    std::vector<float> exportSolutionValues();
private:
    void _setDeviceNumber(int num);
    void _importBounds(float *dest, const float *top, const float *bottom, const float *left, const float *right);
    void _exportBounds(const float *src, float *top, float *bottom, float *left, float *right);
    float _reduceSum(float *mat);
    float _reduceMax(float *mat);
    static float *_allocate_mat(int full_height, int full_width);
    static float *_allocate_arr(int size);
    int stride;
    int height;
    int width;
    CudaMatInfo *mat_info;
    float *mesh_y;
    float *mesh_x;
    float *m_tmp_1;
    float *m_tmp_2;
    float *reduce_buffer_1;
    float *reduce_buffer_2;
    float *buf_top;
    float *buf_bottom;
    float *buf_left;
    float *buf_right;
    float *m_right_part;
    float *m_solution;
    float *m_residual;
    float *m_basis;
};
