#pragma once

class Timer {
public:
    void start();
    void stop();
    double value();
private:
    long long getCurrentUsecs();
    long long _start;
    long long _end;
};
