#pragma once

#include "structs.h"
#include "Communicator.h"
#include "MatChunk.h"

#include "SolverBase.h"

#include <vector>

class SolverCpu : public SolverBase {
public:
    SolverCpu(const ChunkDescription &cd, Communicator &comm, int omp_threads);
    virtual ~SolverCpu() {}
    virtual void solve(float eps, int max_sd_steps = 1, int max_gcm_steps = INT_MAX) override;
    virtual std::vector<float> exportResult() override;

protected:
    MatChunk _right_part, _solution, _residual, _basis;
    float _latest_sp;

    inline float calcLeftPart(const MatChunk &p, int y, int x);
    void computeRightPart();
    void updateSolutionBounds();
    void updateResidualBounds();
    void updateBasisBounds();
    void steepDescentStep(float &error);
    void gcmStep(float &error);
};
