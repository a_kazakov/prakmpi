#pragma once

#include <cuda_runtime_api.h>
#include <cuda.h>

#include "CudaConsts.cuh"

struct CudaMeshPosition {
    __device__ void set(const float *mesh_y_ptr, const float *mesh_x_ptr) {
        hy = *(mesh_y_ptr + 1) - *mesh_y_ptr;
        hx = *(mesh_x_ptr + 1) - *mesh_x_ptr;
        phy = *mesh_y_ptr - *(mesh_y_ptr - 1);
        phx = *mesh_x_ptr - *(mesh_x_ptr - 1);
        hhy = 0.5f * (hy + phy);
        hhx = 0.5f * (hx + phx);
        s_ratio = hhy * hhx;
    };

    float hy, hx, phy, phx, hhy, hhx, s_ratio;
};

struct CudaMatInfo {
    int stride;
    int height, width;
    int full_height, full_width;

    __device__ float &get(float *mat, int y, int x) const {
        return mat[stride * y + x];
    }
    __device__ float get(const float *mat, int y, int x) const {
        return mat[stride * y + x];
    }
    __host__ float *pin(float *mat) const {
        return mat + stride + 1;
    }
    __host__ const float *pin(const float *mat) const {
        return mat + stride + 1;
    }
};

struct NonOverlappedLaunchParams {
    dim3 blocks, threads;
    NonOverlappedLaunchParams(const CudaMatInfo &mat_info) {
        blocks = dim3(
            (mat_info.width + BLOCK_SIZE_2D_X - 1) / BLOCK_SIZE_2D_X,
            (mat_info.height + BLOCK_SIZE_2D_Y - 1) / BLOCK_SIZE_2D_Y
        );
        threads = dim3(BLOCK_SIZE_2D_X, BLOCK_SIZE_2D_Y);
    }
};

struct OverlappedLaunchParams {
    dim3 blocks, threads;
    OverlappedLaunchParams(const CudaMatInfo &mat_info) {
        const int block_eff_size_y = BLOCK_SIZE_2D_Y - 2;
        const int block_eff_size_x = BLOCK_SIZE_2D_X - 2;
        const int n_blocks_y = (mat_info.height + block_eff_size_y - 1) / block_eff_size_y;
        const int n_blocks_x = (mat_info.width + block_eff_size_x - 1) / block_eff_size_x;
        blocks = dim3(n_blocks_x, n_blocks_y);
        threads = dim3(BLOCK_SIZE_2D_X, BLOCK_SIZE_2D_Y);
    }
};
