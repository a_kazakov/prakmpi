// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include <sstream>
#include <iostream>
#include <iomanip>

#define FORCELOGALL(expr) { \
    std::stringstream ss; \
    ss << std::fixed << std::setprecision(6) << expr << std::endl; \
    std::cerr << ss.str() << std::flush; \
}

#define FORCELOG(comm, ...) \
    if (comm.isPrimary()) { \
        FORCELOGALL(__VA_ARGS__); \
    }

#ifndef SILENT
#define LOGALL(expr) { \
    std::stringstream ss; \
    ss << std::fixed << std::setprecision(6) << expr << std::endl; \
    std::cerr << ss.str() << std::flush; \
}

#define LOG(comm, ...) \
    if (comm.isPrimary()) { \
        LOGALL(__VA_ARGS__); \
    }
#else
#define LOG(...)
#define LOGALL(...)
#endif

#ifndef INT_MAX
#define INT_MAX 2147483647
#endif

#define CUDA_CHECK_ERROR(err)           \
    if (err != cudaSuccess) {          \
        LOGALL("Cuda error: " << cudaGetErrorString(err));    \
        LOGALL("Error in file " << __FILE__ << ", line: " << __LINE__);  \
    }
