// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include "stdafx.h"
#include "Communicator.h"

#include <cstring>

Communicator::Communicator(int &argc, char **&argv) {
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    MPI_Comm_rank(MPI_COMM_WORLD, &_wrank);
}

void Communicator::init(int height, int width, bool single_processor) {
    if (single_processor) {
        height = 1;
        width = nprocs;
    }
    _height = height;
    _width = width;
    _single_processor = single_processor;
    int coords[2] = { 0, 0 };
    int dims[2] = { height, width };
    int periods[2] = { 0, 0 };
    MPI_Cart_create(MPI_COMM_WORLD, 2, dims, periods, true, &comm);
    MPI_Comm_rank(comm, &_rank);
    MPI_Cart_coords(comm, _rank, 2, coords);
    MPI_Cart_shift(comm, 1, 1, &_left, &_right);
    MPI_Cart_shift(comm, 0, 1, &_top, &_bottom);
    y = coords[0];
    x = coords[1];
    if (_single_processor) {
        if (_rank > 0) {
            _rank = -1;
        }
        _top = -1;
        _bottom = -1;
        _left = -1;
        _right = -1;
        _width = 1;
        _height = 1;
    }
}

void Communicator::finalize() {
    MPI_Finalize();
}

int Communicator::getHostRank() {
    if (_single_processor) {
        return 0;
    }
    int size = _height * _width;
    std::vector<char *> hostnames(size);
    for (int i = 0; i < size; ++i) {
        hostnames[i] = new char[MPI_MAX_PROCESSOR_NAME];
    }
    int host_length;
    MPI_Get_processor_name(hostnames[_rank], &host_length);
    for (int i = 0; i < size; ++i) {
        MPI_Bcast(hostnames[i], MPI_MAX_PROCESSOR_NAME, MPI_CHAR, i, comm);
    }
    int result = 0;
    for (int i = 0; i < _rank; ++i) {
        if (strncmp(hostnames[i], hostnames[_rank], MPI_MAX_PROCESSOR_NAME) == 0) {
            ++result;
        }
    }
    for (int i = 0; i < size; ++i) {
        delete [] hostnames[i];
    }
    return result;
}

void Communicator::sendToNeighbours(
    int top_length,
    float *top_values,
    int bottom_length,
    float *bottom_values,
    int left_length,
    float *left_values,
    int right_length,
    float *right_values
) {
    // Send and forget
    sendIfHasNeighbour(_top, TOP, top_length, top_values);
    sendIfHasNeighbour(_bottom, BOTTOM, bottom_length, bottom_values);
    sendIfHasNeighbour(_left, LEFT, left_length, left_values);
    sendIfHasNeighbour(_right, RIGHT, right_length, right_values);
}

void Communicator::recvFromNeighbours(
    int top_length,
    float *top_values,
    int bottom_length,
    float *bottom_values,
    int left_length,
    float *left_values,
    int right_length,
    float *right_values
) {
    MPI_Request reqs[4];
    int req_cnt = 0;
    req_cnt += recvIfHasNeighbour(_top, BOTTOM, top_length, top_values, reqs[req_cnt]);
    req_cnt += recvIfHasNeighbour(_bottom, TOP, bottom_length, bottom_values, reqs[req_cnt]);
    req_cnt += recvIfHasNeighbour(_left, RIGHT, left_length, left_values, reqs[req_cnt]);
    req_cnt += recvIfHasNeighbour(_right, LEFT, right_length, right_values, reqs[req_cnt]);
    if (req_cnt > 0) {
        MPI_Waitall(req_cnt, reqs, MPI_STATUS_IGNORE);
    }
}

float Communicator::reduceMaximum(float value) {
    if (_single_processor) {
        return value;
    }
    float result;
    MPI_Allreduce(&value, &result, 1, MPI_FLOAT, MPI_MAX, comm);
    return result;
}

float Communicator::reduceSum(float value) {
    if (_single_processor) {
        return value;
    }
    float result;
    MPI_Allreduce(&value, &result, 1, MPI_FLOAT, MPI_SUM, comm);
    return result;
}

void Communicator::barrier() {
    if (_single_processor) {
        return;
    }
    MPI_Barrier(comm);
}

void Communicator::sendResult(std::vector<float>& data) {
    if (_single_processor) {
        return;
    }
    int size = int(data.size());
    MPI_Request req1;
    MPI_Isend(
        reinterpret_cast<void *>(&size),
        1,
        MPI_INT,
        0,
        static_cast<int>(GATHER_SIZE),
        comm,
        &req1
    );
    MPI_Send(
        reinterpret_cast<void *>(&data.front()),
        size,
        MPI_FLOAT,
        0,
        static_cast<int>(GATHER_DATA),
        comm
    );
    MPI_Request_free(&req1);
}

std::vector<float> Communicator::fetchResult() {
    if (_single_processor) {
        return std::vector<float>();
    }
    MPI_Status st1;
    int size;
    MPI_Recv(
        reinterpret_cast<void *>(&size), 
        1, 
        MPI_INT, 
        MPI_ANY_SOURCE, 
        static_cast<int>(GATHER_SIZE), 
        comm, 
        &st1
    );
    std::vector<float> result(size);
    MPI_Recv(
        reinterpret_cast<void *>(&result.front()), 
        size,
        MPI_FLOAT, 
        st1.MPI_SOURCE, 
        static_cast<int>(GATHER_DATA), 
        comm, 
        MPI_STATUSES_IGNORE
    );
    return result;
}

void Communicator::sendIfHasNeighbour(int neighbour_rank, Communicator::Tag tag, int length, float *values) {
    if (neighbour_rank < 0) {
        return;
    }
    MPI_Request req;
    MPI_Isend(
        reinterpret_cast<void *>(values),
        length,
        MPI_FLOAT,
        neighbour_rank,
        static_cast<int>(tag),
        comm,
        &req
    );
    MPI_Request_free(&req);
}

bool Communicator::recvIfHasNeighbour(int neighbout_rank, Tag tag, int length, float *values, MPI_Request &req) {
    if (neighbout_rank < 0) {
        return false;
    }
    MPI_Irecv(
        reinterpret_cast<void *>(values),
        length,
        MPI_FLOAT,
        neighbout_rank,
        static_cast<int>(tag),
        comm,
        &req
    );
    return true;
}
