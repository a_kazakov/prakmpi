// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include "stdafx.h"
#include "SolverCpu.h"

#include <algorithm>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <omp.h>

SolverCpu::SolverCpu(const ChunkDescription &cd, Communicator &comm, int omp_threads) :
    SolverBase(cd, comm),
    _right_part(cd),
    _solution(cd),
    _residual(cd),
    _basis(cd),
    _latest_sp(0)
{
    omp_set_num_threads(omp_threads);
    computeRightPart();
    // for(int i = 0; i < 150; ++i) std::cerr << _right_part._mat[i] << " "; std::cerr << std::endl;
    // LOGALL("Offsets: " << _right_part._yoffset << " " << _right_part._xoffset);
}

inline float SolverCpu::calcLeftPart(const MatChunk &p, int y, int x) {
    return (
        (
            (p(y, x) - p(y - 1, x)) / _mesh.hy(y - 1) - (p(y + 1, x) - p(y, x)) / _mesh.hy(y)
        ) / _mesh.hhy(y) +
        (
            (p(y, x) - p(y, x - 1)) / _mesh.hx(x - 1) - (p(y, x + 1) - p(y, x)) / _mesh.hx(x)
        ) / _mesh.hhx(x)
    );
}

void SolverCpu::computeRightPart() {
#pragma omp parallel for
    for (int i = _cd.top_node; i <= _cd.bottom_node; ++i) {
        for (int j = _cd.left_node; j <= _cd.right_node; ++j) {
            _right_part(i, j) = get_right_part(_mesh.get_vpoint(i), _mesh.get_hpoint(j));
        }
    }
}

void SolverCpu::updateSolutionBounds() {
    _solution.exportBounds(
        getBuffer(PREV_SOLUTION_TOP),
        getBuffer(PREV_SOLUTION_BOTTOM),
        getBuffer(PREV_SOLUTION_LEFT),
        getBuffer(PREV_SOLUTION_RIGHT)
    );
    _exchangeSolutionBounds();
    _solution.importBounds(
        getBuffer(NEXT_SOLUTION_TOP),
        getBuffer(NEXT_SOLUTION_BOTTOM),
        getBuffer(NEXT_SOLUTION_LEFT),
        getBuffer(NEXT_SOLUTION_RIGHT)
    );
}

void SolverCpu::updateResidualBounds() {
    _residual.exportBounds(
        getBuffer(PREV_RESIDUAL_TOP),
        getBuffer(PREV_RESIDUAL_BOTTOM),
        getBuffer(PREV_RESIDUAL_LEFT),
        getBuffer(PREV_RESIDUAL_RIGHT)
    );
    _exchangeResidualBounds();
    _residual.importBounds(
        getBuffer(NEXT_RESIDUAL_TOP),
        getBuffer(NEXT_RESIDUAL_BOTTOM),
        getBuffer(NEXT_RESIDUAL_LEFT),
        getBuffer(NEXT_RESIDUAL_RIGHT)
    );
}

void SolverCpu::updateBasisBounds() {
    _basis.exportBounds(
        getBuffer(PREV_BASIS_TOP),
        getBuffer(PREV_BASIS_BOTTOM),
        getBuffer(PREV_BASIS_LEFT),
        getBuffer(PREV_BASIS_RIGHT)
    );
    _exchangeBasisBounds();
    _basis.importBounds(
        getBuffer(NEXT_BASIS_TOP),
        getBuffer(NEXT_BASIS_BOTTOM),
        getBuffer(NEXT_BASIS_LEFT),
        getBuffer(NEXT_BASIS_RIGHT)
    );
}

void SolverCpu::steepDescentStep(float &current_error) {
    updateSolutionBounds();
#pragma omp parallel for
    for (int i = _cd.top_node; i <= _cd.bottom_node; ++i) {
        for (int j = _cd.left_node; j <= _cd.right_node; ++j) {
            _residual(i, j) = calcLeftPart(_solution, i, j) - _right_part(i, j);
        }
    }
    updateResidualBounds();
    float sp1 = 0.0, sp2 = 0.0;
#pragma omp parallel for reduction(+:sp1) reduction(+:sp2)
    for (int i = _cd.top_node; i <= _cd.bottom_node; ++i) {
        for (int j = _cd.left_node; j <= _cd.right_node; ++j) {
            const float res = _residual(i, j);
            const float sr = _mesh.s_ratio(i, j);
            sp1 += res * res * sr;
            sp2 += calcLeftPart(_residual, i, j) * res * sr;
        }
    }
    sp1 = _comm.reduceSum(sp1);
    sp2 = _comm.reduceSum(sp2);
    const float tau = sp1 / sp2;
    _latest_sp = sp2;
    error = 0.0;
#pragma omp parallel
    {
        float thread_error = 0.0;
#pragma omp for
        for (int i = _cd.top_node; i <= _cd.bottom_node; ++i) {
            for (int j = _cd.left_node; j <= _cd.right_node; ++j) {
                const float next_value = _solution(i, j) - tau * _residual(i, j);
                thread_error = std::max(thread_error, std::fabs(next_value - _solution(i, j)));
                _solution(i, j) = next_value;
            }
        }
#pragma omp critical
        current_error = std::max(current_error, thread_error);
    }
}

void SolverCpu::gcmStep(float &current_error) {
    updateSolutionBounds();
#pragma omp parallel for
    for (int i = _cd.top_node; i <= _cd.bottom_node; ++i) {
        for (int j = _cd.left_node; j <= _cd.right_node; ++j) {
            _residual(i, j) = calcLeftPart(_solution, i, j) - _right_part(i, j);
        }
    }
    updateResidualBounds();
    float alpha = 0.0;
#pragma omp parallel for reduction(+:alpha)
    for (int i = _cd.top_node; i <= _cd.bottom_node; ++i) {
        for (int j = _cd.left_node; j <= _cd.right_node; ++j) {
            const float sr = _mesh.s_ratio(i, j);
            alpha += calcLeftPart(_residual, i, j) * _basis(i, j) * sr;
        }
    }
    alpha = _comm.reduceSum(alpha);
    alpha /= _latest_sp;
#pragma omp parallel for
    for (int i = _cd.top_node; i <= _cd.bottom_node; ++i) {
        for (int j = _cd.left_node; j <= _cd.right_node; ++j) {
            _basis(i, j) = _residual(i, j) - alpha *_basis(i, j);
        }
    }
    updateBasisBounds();
    float sp1 = 0.0, sp2 = 0.0;
#pragma omp parallel for reduction(+:sp1) reduction(+:sp2)
    for (int i = _cd.top_node; i <= _cd.bottom_node; ++i) {
        for (int j = _cd.left_node; j <= _cd.right_node; ++j) {
            const float sr = _mesh.s_ratio(i, j);
            sp1 += _residual(i, j) * _basis(i, j) * sr;
            sp2 += calcLeftPart(_basis, i, j) * _basis(i, j) * sr;
        }
    }
    sp1 = _comm.reduceSum(sp1);
    sp2 = _comm.reduceSum(sp2);
    const float tau = sp1 / sp2;
    _latest_sp = sp2;
    current_error = 0.0;
#pragma omp parallel
    {
        float thread_error = 0.0;
#pragma omp for
        for (int i = _cd.top_node; i <= _cd.bottom_node; ++i) {
            for (int j = _cd.left_node; j <= _cd.right_node; ++j) {
                const float next_value = _solution(i, j) - tau * _basis(i, j);
                thread_error = std::max(thread_error, std::fabs(next_value - _solution(i, j)));
                _solution(i, j) = next_value;
            }
        }
#pragma omp critical
        current_error = std::max(current_error, thread_error);
    }
}

void SolverCpu::solve(float eps, int max_sd_steps, int max_gcm_steps) {
    error = INFINITY;
    steps = 0;
    for (int i = 0; i < max_sd_steps && error > eps; ++i) {
        ++steps;
        steepDescentStep(error);
        error = _comm.reduceMaximum(error);
        LOG(_comm, "Performed step " << steps << " (algo=SD, error=" << error << ")");
    }
    _basis.assign(_residual);
    for (int i = 0; i < max_gcm_steps && error > eps; ++i) {
        ++steps;
        gcmStep(error);
        error = _comm.reduceMaximum(error);
        LOG(_comm, "Performed step " << steps << " (algo=GCM, error=" << error << ")");
    }
}

std::vector<float> SolverCpu::exportResult() {
    std::vector<float> result;
    result.reserve(3 * _cd.hnodes() * _cd.vnodes());
    for (int i = _cd.top_node; i <= _cd.bottom_node; ++i) {
        for (int j = _cd.left_node; j <= _cd.right_node; ++j) {
            result.push_back(_mesh.get_vpoint(i));
            result.push_back(_mesh.get_hpoint(j));
            result.push_back(_solution(i, j));
        }
    }
    return result;
}
