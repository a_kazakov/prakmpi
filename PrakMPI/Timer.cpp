// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include "stdafx.h"
#include "Timer.h"

#if defined(_WIN32) || defined(_WIN64) 

#include <windows.h>
#include <stdint.h>

int gettimeofday(struct timeval *tp, struct timezone *tzp)
{
    // Note: some broken versions only have 8 trailing zero's, the correct epoch has 9 trailing zero's
    static const uint64_t EPOCH = ((uint64_t)116444736000000000ULL);

    SYSTEMTIME  system_time;
    FILETIME    file_time;
    uint64_t    time;

    GetSystemTime(&system_time);
    SystemTimeToFileTime(&system_time, &file_time);
    time = ((uint64_t)file_time.dwLowDateTime);
    time += ((uint64_t)file_time.dwHighDateTime) << 32;

    tp->tv_sec = (long)((time - EPOCH) / 10000000L);
    tp->tv_usec = (long)(system_time.wMilliseconds * 1000);
    return 0;
}

#else

#include <sys/time.h>

#endif

void Timer::start() {
    _start = getCurrentUsecs();
}

void Timer::stop() {
    _end = getCurrentUsecs();
}

double Timer::value() {
    return double(_end - _start) / 1000000.0;
}

long long Timer::getCurrentUsecs() {
    timeval tv;
    if (gettimeofday(&tv, 0) != 0) {
        return 0;
    }
    return 1000000ULL * tv.tv_sec + tv.tv_usec;
}
