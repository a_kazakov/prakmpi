#pragma once

static const int BLOCK_SIZE_1D = 512;
static const int BLOCK_SIZE_2D_Y = 16;
static const int BLOCK_SIZE_2D_X = 32;
static const int REDUCER_BLOCKS = 64;
