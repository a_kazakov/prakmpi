// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include "stdafx.h"
#include "TaskSplitter.h"

#include <stdexcept>

TaskSplitter::TaskSplitter(int v_max_node, int h_max_node, int nprocs) : 
    v_max_node(v_max_node), 
    h_max_node(h_max_node) 
{
    int vpow = 0, hpow = 0;
    int npows = getLog2(nprocs);
    int vnodes = v_max_node - 2;
    int hnodes = h_max_node - 2;
    for (int i = 0; i < npows; i++) {
        if (vnodes> hnodes) {
            vnodes = (vnodes + 1) / 2;
            ++vpow;
        } else {
            hnodes = (hnodes + 1) / 2;
            ++hpow;
        }
    }
    vchunks = 1 << vpow;
    hchunks = 1 << hpow;
}

ChunkDescription TaskSplitter::createChunkDescription(int y, int x, float top_value, float bottom_value, float left_value, float right_value) {
    ChunkDescription cd;
    cd.g_top_value = top_value;
    cd.g_bottom_value = bottom_value;
    cd.g_left_value = left_value;
    cd.g_right_value = right_value;
    cd.top_node = 1 + (v_max_node - 1) * y / vchunks;
    cd.bottom_node = (v_max_node - 1) * (y + 1) / vchunks;
    cd.left_node = 1 + (h_max_node - 1) * x / hchunks;
    cd.right_node = (h_max_node - 1) * (x + 1) / hchunks;
    cd.g_bottom_node = v_max_node;
    cd.g_right_node = h_max_node;
    return cd;
}

int TaskSplitter::getLog2(int value) {
    int result = 0;
    while (value > 1) {
        if (value & 1) {
            throw std::runtime_error("Number of processors should be power of two");
        }
        ++result;
        value /= 2;
    }
    return result;
}
