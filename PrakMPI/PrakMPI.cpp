// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

// PrakMPI.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "ArgParser.h"
#include "structs.h"
#include "taskdef.h"
#include "Communicator.h"
#include "TaskSplitter.h"
#include "SolverCpu.h"
#include "SolverCuda.h"
#include "SolverBase.h"
#include "Timer.h"

#include <string>
#include <stdexcept>
#include <fstream>
#include <memory>

int main(int argc, char *argv[]) {
    Communicator comm(argc, argv);
    try {
        // Parse arguments
        ArgParser args(argc, argv);
        const int v_size = args.getMeshHeight();
        const int h_size = args.getMeshWidth();
        const int max_sd_steps = args.getMaxSDSteps();
        const int max_gcm_steps = args.getMaxGCMSteps();
        const std::string output = args.getOutputFileName();
        const bool single_processor = args.useSingleProcessor();
        const int omp_threads = args.getOmpThreads();
        const bool use_cuda = args.useCuda();
        // Prepare data
        TaskSplitter ts(v_size - 1, h_size - 1, single_processor ? 1 : comm.nprocs);
        comm.init(ts.vchunks, ts.hchunks, single_processor);
        if (!comm.isActive()) {
            throw 1; // Go to finalize
        }
        if (use_cuda) {
            FORCELOG(comm, 
                "Starting params: " <<
                "\n     nprocs=" << comm.nprocs << (single_processor ? " (single processor mode)" : "") <<
                "\n     procs_topology=" << ts.hchunks << "x" << ts.vchunks <<
                "\n     device=CUDA" <<
                "\n     mesh_size=" << v_size << "x" << h_size <<
                "\n     sd_steps=" << max_sd_steps << 
                "\n     gcm_steps=" << max_gcm_steps << "\n");
        } else {
            FORCELOG(comm, 
                "Starting params: " <<
                "\n     nprocs=" << comm.nprocs << (single_processor ? " (single processor mode)" : "") <<
                "\n     procs_topology=" << ts.hchunks << "x" << ts.vchunks <<
                "\n     device=CPU (omp_threads=" << omp_threads << ")"
                "\n     mesh_size=" << v_size << "x" << h_size <<
                "\n     sd_steps=" << max_sd_steps << 
                "\n     gcm_steps=" << max_gcm_steps << "\n");
        }
        ChunkDescription cd = ts.createChunkDescription(comm.y, comm.x, LOW_Y, HIGH_Y, LOW_X, HIGH_X);
        Timer timer;
        std::shared_ptr<SolverBase> solver;
        if (use_cuda) {
            solver = std::make_shared<SolverCuda>(cd, comm);
        } else {
            solver = std::make_shared<SolverCpu>(cd, comm, omp_threads);
        }
        comm.barrier(); // Wait till all nodes are ready for precise time measurement
        // Solve and measure time
        timer.start();
        solver->solve(RESULT_EPS, max_sd_steps, max_gcm_steps);
        timer.stop();
        // Process results
        float total_time = comm.reduceMaximum(float(timer.value()));
        FORCELOG(comm, "Computation error: " << solver->error);
        FORCELOG(comm, "Computational steps performed: " << solver->steps);
        FORCELOG(comm, "Computation time: " << total_time << " seconds");
        if (!output.empty()) {
            timer.start();
            std::vector<float> result = solver->exportResult();
            if (comm.isPrimary()) {
                std::ofstream os(output.c_str());
                for (int i = 0; i < comm.nprocs; ++i) {
                    if (i != 0) {
                        result = comm.fetchResult();
                    }
                    for (int i = 0; i < result.size(); i += 3) {
                        os << result[i] << " " << result[i + 1] << " " << result[i + 2] << "\n";
                    }
                }
            } else {
                comm.sendResult(result);
            }
            timer.stop();
            FORCELOG(comm, "Saving time: " << timer.value() << " seconds");
        }
    } catch (int) {
        // pass
    } catch (std::exception &ex) {
        FORCELOG(comm, ex.what());
    } catch (...) {
        FORCELOG(comm, "Unknown error occured");
    }
    comm.finalize();
    return 0;
}
