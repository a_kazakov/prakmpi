#pragma once

#include <cuda_runtime_api.h>
#include <cuda.h>

__device__ static float cuda_get_right_part(float y, float x) {
    return (y * y + x * x) * sin(y * x);
}
