#pragma once

#include "structs.h"

class TaskSplitter {
public:
    int vchunks, hchunks;
    int v_max_node, h_max_node;
    TaskSplitter(int v_max_node, int h_max_node, int nprocs);
    ChunkDescription createChunkDescription(
        int y, 
        int x, 
        float top_value, 
        float bottom_value, 
        float left_value, 
        float right_value
    );
private:
    int getLog2(int value);
};
