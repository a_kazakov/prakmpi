#pragma once

#include "SolverBase.h"
#include "CudaProcessor.h"

class SolverCuda : public SolverBase {
public:
    SolverCuda(const ChunkDescription &cd, Communicator &comm);
    virtual ~SolverCuda() {};
    virtual void solve(float eps, int max_sd_steps = 1, int max_gcm_steps = INT_MAX) override;
    virtual std::vector<float> exportResult() override;
protected:
    CudaProcessor _cp;
    float _latest_sp;
    void updateSolutionBounds();
    void updateResidualBounds();
    void updateBasisBounds();
    void steepDescentStep(float &error);
    void gcmStep(float &error);
};
