#pragma once

#include "CudaFunctions.cuh"

#define NON_OVERLAPPED_GRID \
    int global_y, global_x, thread_y, thread_x; \
    bool is_active; \
    _dLocateNonOverlappedPosition(mat_info, global_y, global_x, thread_y, thread_x, is_active);

#define OVERLAPPED_GRID \
    int global_y, global_x, thread_y, thread_x; \
    bool is_active, is_border; \
    _dLocateOverlappedPosition(mat_info, global_y, global_x, thread_y, thread_x, is_active, is_border);
