#pragma once

#include <cmath>

static float get_border_value(float y, float x) {
    return 1.0f + std::sin(y * x);
}

static float get_right_part(float y, float x) {
    return (y * y + x * x) * std::sin(y * x);
}

static const float LOW_X = 0;
static const float LOW_Y = 0;
static const float HIGH_X = 2;
static const float HIGH_Y = 2;

static const float RESULT_EPS = 1e-4f;
