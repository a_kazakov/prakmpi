#pragma once

struct ChunkDescription {
    float g_top_value;
    float g_bottom_value;
    float g_left_value;
    float g_right_value;
    int top_node;
    int bottom_node;
    int left_node;
    int right_node;
    int g_bottom_node;
    int g_right_node;

    inline int vnodes() const {
        return bottom_node - top_node + 1;
    }
    inline int hnodes() const {
        return right_node - left_node + 1;
    }
    inline bool idle() const {
        return top_node > bottom_node || left_node > right_node;
    }
};
